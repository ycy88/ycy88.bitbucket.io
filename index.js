(function() {
  var GiphyApp = angular.module("GiphyApp", []);

  var GiphyController = function($http) {
    var giphyController = this;

    giphyController.subject = "Key in something";
    giphyController.image = "http://www.jennybeaumont.com/wp-content/uploads/2015/03/placeholder.gif";
    giphyController.search = "test";

    giphyController.search = function() {
      if (!giphyController.subject){
        return
      } console.log('in search : %s', giphyController.subject);
      var promise = $http.get("http://api.giphy.com/v1/gifs/random", {
        params: {
          api_key: "dc6zaTOxFJmzC",
          tag: giphyController.subject
        }
      });
      promise.then(function(result){
        giphyController.image = result.data.data.image_url;
      });
    };


  };

  GiphyController.$inject = [ "$http" ];

  GiphyApp.controller("GCtrl", GiphyController);

})();